let userData = {
    USD: 1000,
    EUR: 900,
    UAH: 15000,
    BIF: 20000,
    AOA: 100,
  },
  bankData = {
    USD: {
      max: 3000,
      min: 100,
      img: "💵",
    },
    EUR: {
      max: 1000,
      min: 50,
      img: "💶",
    },
    UAH: {
      max: 0,
      min: 0,
      img: "💴",
    },
    BIF: {
      max: 10000,
      min: 100,
      img: "💷",
    },
  };

function getCurrency(data) {
  let currency;

  do {
    currency = prompt(
      `Введите название валюты в формате: ${data}`
    )?.toUpperCase();
  } while (!data.includes(currency));
  console.log(currency);
  return currency;
}
function getSum(balance, currency, info) {
  let sum = prompt("Введите сумму");

  if (sum > balance || sum > info.max) {
    console.log(
      `Введенная сумма больше допустимой. Максимальная сумма снятия: ${balance} ${currency}`
    );
  }
  if (sum < info.min) {
    console.log(
      `Введенная сумма меньше допустимой. Минимальная сумма снятия: ${info.min} ${currency}`
    );
  }
  if (sum >= info.min && sum <= balance) {
    console.log(`Вот Ваши денежки ${sum} ${currency} ${info.img}`);
  }
}

function getMoney() {
  return new Promise((resolve, reject) => {
    confirm("Посмотреть баланс на карте?")
      ? resolve(userData)
      : reject({ userData: userData, bankData: bankData });
  });
}

getMoney()
  .then(() => {
    const value = Object.keys(userData);
    const currency = getCurrency(value);

    console.log(`Баланс составляет:${userData[currency]} ${currency}`);
  })
  .catch(() => {
    let value = Object.keys(userData);
    let validCurrency = value.filter((elem) => bankData[elem]);
    const currency = getCurrency(validCurrency);

    getSum(userData[currency], currency, bankData[currency]);
  })
  .finally(() => {
    console.log("Спасибо, хорошего дня 😊");
  });
